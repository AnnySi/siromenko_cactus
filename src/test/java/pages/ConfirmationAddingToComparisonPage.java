package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfirmationAddingToComparisonPage extends BasePage {
    public ConfirmationAddingToComparisonPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//a[@class='fancybox-item fancybox-close']")
    public WebElement closeXIcon;

    public ProductListPage clickCloseIcon() {
        closeXIcon.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//a[@class='comparison_link']")
    public WebElement transitionToComparativePage;

    public ComparativePage clickOnTransitionToComperative() {
        transitionToComparativePage.click();
        return new ComparativePage(driver);
    }
}
