package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrderPage extends BasePage {
    public OrderPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//*[@class=\"layer_cart_product col-xs-12 col-md-8\"]/span[@class='title']")
    //("//*[@id=\"layer_cart\"]/div[1]")
    public WebElement titleInConfirmationWindow;

    @FindBy(xpath = "//span[@id='layer_cart_product_title']")
    public WebElement descriptionInItemTitleOrderPage;

}
