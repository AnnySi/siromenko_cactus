package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComparativePage extends BasePage {
    public ComparativePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//h1[@class='lead-title text-center cart-empty']")
    public WebElement emptyCompare;
}
