package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductListPage extends BasePage {
    public ProductListPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//*[@id='title']")
    public WebElement contentOfCorrespondingItem;
    @FindBy(xpath = "//*[@class='subcategory-image']/a[@title='Apple Watch']")
    public WebElement appleWatch;
    @FindBy(xpath = "//*[@class='subcategory-image']/a[@title='Apple Watch Series 4']")
    public WebElement appleWatchSeries4;
    @FindBy(xpath = "//*[@class='image']/a[contains(@title,'MU682')]")
    public WebElement appleWatchSeries4WithPrice;

    public ProductListPage clickAppleWatch() {
        appleWatch.click();
        return new ProductListPage(driver);
    }

    public ProductListPage clickAppleWatchSeries4() {
        appleWatchSeries4.click();
        return new ProductListPage(driver);
    }

    public ProductPage clickAppleWatchSeries4WithPrice() {
        appleWatchSeries4WithPrice.click();
        return new ProductPage(driver);
    }

    @FindBy(xpath = "//div[@class='subcategory-image']//img[contains(@src,'umnye')]")
    public WebElement smartClockCategory;

    public ProductListPage clickSmartClockCategory() {
        smartClockCategory.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//*[@id=\"product_list\"]//a[@class='add_to_compare btn-add-to-compare ' and @data-id-product='7165']")
    public WebElement iconOfWeigherBeforSelection;

    public ConfirmationAddingToComparisonPage clickIconOfWeigherBeforSelection() {
        iconOfWeigherBeforSelection.click();
        return new ConfirmationAddingToComparisonPage(driver);
    }

    @FindBy(xpath = "//a[@data-id-product='7165' and @title='Сравнить']")//"//*[@id=\"product_list\"]//a[@class='add_to_compare btn-add-to-compare  checked' and @data-id-product='7165']")
    public WebElement iconOfWeigherChecked;

    public ConfirmationAddingToComparisonPage clickIconOfCheckedWeigher() {
        iconOfWeigherChecked.click();
        return new ConfirmationAddingToComparisonPage(driver);
    }
}
