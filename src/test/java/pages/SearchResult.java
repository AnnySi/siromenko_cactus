package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResult extends BasePage {


    @FindBy(xpath = "//*[@class='col-xs-12 col-sm-6 col-md-4 no-margin product-item-holder small hover'][1]")
    public WebElement searchResultFindText;
    @FindBy(xpath = "//*[@class='alert alert-warning']")
    public WebElement searchNoResultFindText;



    public SearchResult(WebDriver webDriver) {
        super(webDriver);
    }

    public Header getHeader() {
        return new Header(driver);
    }
}
