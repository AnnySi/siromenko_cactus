package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header extends BasePage {
    public Header(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id='search_query_block']")
    public WebElement searchField;

    @FindBy(xpath = "//*[@id='title']")
    public WebElement contentOfCorrespondingItem2;

    public Header enterInputText(String inputText) {
        searchField.sendKeys(inputText);
        return this;
    }

    public SearchResult performSearch() {
        searchField.sendKeys(Keys.ENTER);
        return new SearchResult(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Смартфоны']")
    WebElement menuItemSmartfony;

    public ProductListPage clickItemSmartphone() {
        menuItemSmartfony.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Планшеты, ноутбуки']")
    WebElement menuItemPlanshety;

    public ProductListPage clickItemTablets() {
        menuItemPlanshety.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Часы']")
    WebElement menuItemChasy;

    public ProductListPage clickItemClock() {
        menuItemChasy.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Фитнес и здоровье']")
    WebElement menuItemFitnes;

    public ProductListPage clickItemFitness() {
        menuItemFitnes.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Умный дом']")
    WebElement menuItemDom;

    public ProductListPage clickItemSmartHome() {
        menuItemDom.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Батареи']")
    WebElement menuItemBatarei;

    public ProductListPage clickItemBattery() {
        menuItemBatarei.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Развлечения']")
    WebElement menuItemRazvlechenia;

    public ProductListPage clickItemEntertainment() {
        menuItemRazvlechenia.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Фото и видео']")
    WebElement menuItemPhoto;

    public ProductListPage clickItemPhoto() {
        menuItemPhoto.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Аудио']")
    WebElement menuItemAudio;

    public ProductListPage clickItemAudio() {
        menuItemAudio.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//div[@class='menu-container']//a[@target='_parent' and @title='Аксессуары']")
    WebElement menuItemAcssesority;

    public ProductListPage clickItemAccessories() {
        menuItemAcssesority.click();
        return new ProductListPage(driver);
    }

    @FindBy(xpath = "//*[@id=\"header_link_delivery\"]/a[contains(@href,'delivery')]")
    WebElement menuItemDelivery;

    public DeliveryPage clickItemDelivery() {
        menuItemDelivery.click();
        return new DeliveryPage(driver);
    }

    @FindBy(xpath = "//div[@class=\"compare-header\"]//img")
    public WebElement compareIcon;

    public ComparativePage clickCompareIcon() {
        compareIcon.click();
        return new ComparativePage(driver);
    }

}
