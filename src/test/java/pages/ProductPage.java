package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//*[@id='title']")
    public WebElement productTitle;

    @FindBy(xpath ="//*[@id=\"add_to_cart\"]/button")
    public  WebElement orderButton;

    public OrderPage clickOrderButton(){
        orderButton.click();
        return new OrderPage(driver);
    }


}
