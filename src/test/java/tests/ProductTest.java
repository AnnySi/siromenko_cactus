package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Header;
import pages.ProductListPage;
import pages.ProductPage;
import pages.SearchResult;

public class ProductTest extends BaseTest {

    @Test(description = "Navigate To Product Details Page")
    public void navigateToProductDetailsPage() {
        driver.get("https://cactus.kh.ua/");

        ProductListPage productListPage = new Header(driver)
                .clickItemClock()
                .clickAppleWatch()
                .clickAppleWatchSeries4();
        ProductPage productPage = productListPage.clickAppleWatchSeries4WithPrice();

        Assert.assertEquals(productPage.productTitle.getText(), "Apple Watch Series 4 (GPS) 40mm Gold Aluminum Case with Pink Sand Sport Band MU682", "Product is not displayed");
    }

    @Test(description = "Navigate To Product Details Page with order button")
    public void navigateToProductDetailsPageWithOrderButton() {
        driver.get("https://cactus.kh.ua/");

        ProductListPage productListPage = new Header(driver)
                .clickItemClock()
                .clickAppleWatch()
                .clickAppleWatchSeries4();
        ProductPage productPage = productListPage.clickAppleWatchSeries4WithPrice();

        Assert.assertTrue(productPage.orderButton.isDisplayed(), "It isn't displayed");
    }

}
