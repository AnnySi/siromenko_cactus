package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.Header;
import pages.SearchResult;

import java.util.concurrent.TimeUnit;

public class SearchTests extends BaseTest {
    /* WebDriver driver;

    @BeforeMethod
    public void createBrowser() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void closeBrowser() {
        if (driver != null) {
            driver.quit();
        }
    }*/

    @DataProvider
    public Object[][] getInputTexts() {
        return new Object[][]{
                {"smart"},
                {"TV"}
        };
    }

    @Test(dataProvider = "getInputTexts", description = "Positive search case")
    public void executeSearch(String inputText) {
        driver.get("https://cactus.kh.ua/");

        SearchResult searchResult = new Header(driver)
                .enterInputText(inputText)
                .performSearch();
        Assert.assertTrue(searchResult.searchResultFindText.isDisplayed(), "Result was not found");
    }

    @Test(description = "Negative search cases")
    public void negativeSearch() {
        driver.get("https://cactus.kh.ua/");

        SearchResult searchResult = new Header(driver)// єто то же, что и следующий коммент,с учетом класса Header
                .enterInputText("hghghgg")
                .performSearch();

        Assert.assertTrue(searchResult.searchNoResultFindText.isDisplayed(), "Search was NOT executed");

    }
}
