package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;

public class CompareTest extends BaseTest {

    @Test(description = "Empty compare test")
    public void emptyCompareTest() {
        driver.get("https://cactus.kh.ua/");

        ComparativePage comparativePage = new Header(driver)
                .clickCompareIcon();

        Assert.assertEquals(comparativePage.emptyCompare.getText(),"Вы не добавили для сравнения ни одного товара", "Page is not empty");
    }

    @Test(description = "Check Compare page after delete item from compare")
    public void  checkAfterDeleteItemFromCompare() throws InterruptedException {
        driver.get("https://cactus.kh.ua/");
        ConfirmationAddingToComparisonPage confirmationAddingToComparisonPage=new Header(driver)
                .clickItemSmartHome()
                .clickSmartClockCategory()
                .clickIconOfWeigherBeforSelection();
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOf(confirmationAddingToComparisonPage.closeXIcon/*By.xpath("//a[@class='fancybox-item fancybox-close']")*/));
               ProductListPage productListPage= confirmationAddingToComparisonPage.clickCloseIcon();
               Thread.sleep(1000);
        ConfirmationAddingToComparisonPage confirmationAddingToComparisonPage1=productListPage.clickIconOfCheckedWeigher();
               ComparativePage comparativePage=confirmationAddingToComparisonPage1.clickOnTransitionToComperative();

        Assert.assertEquals(comparativePage.emptyCompare.getText(),"Вы не добавили для сравнения ни одного товара", "Page is not empty");
    }
}
