package tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.DeliveryPage;
import pages.Header;

public class DeliveryTest extends BaseTest {

    @Test(description = "Find out info about Delivery")
    public void navigateToDeliveryPage(){
        driver.get("https://cactus.kh.ua/");
        DeliveryPage  deliveryPage=new Header(driver)
                .clickItemDelivery();
        Assert.assertEquals(deliveryPage.deliveryInfoTitle.getText(),"Доставка компанией НОВАЯ ПОЧТА", "Info is not existed");

    }
}
