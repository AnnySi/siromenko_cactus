package tests;


import org.testng.Assert;

import org.testng.annotations.Test;
import pages.Header;
import pages.ProductListPage;


public class NavigationTests extends BaseTest {

    @Test(description = "Check content of page after click menu item Смартфоны ")
    public void checkContentAfterClickItemSmartphone() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemSmartphone();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }


    @Test(description = "Check content of page after click menu item Планшеты, ноутбуки")
    public void checkContentAfterClickItemTablets() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemTablets();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Часы ")
    public void checkContentAfterClickItemClock() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemClock();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Фитнес и здоровье ")
    public void checkContentAfterClickItemFitness() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemFitness();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Умный дом ")
    public void checkContentAfterClickItemSmartHome() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemSmartHome();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Батареи ")
    public void checkContentAfterClickItemBattery() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemBattery();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Развлечения  ")
    public void checkContentAfterClickItemEntertainment() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemEntertainment();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Фото и видео ")
    public void checkContentAfterClickItemPhoto() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemPhoto();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Аудио  ")
    public void checkContentAfterClickItemAudio() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemAudio();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

    @Test(description = "Check content of page after click menu item Аксессуары  ")
    public void checkContentAfterClickItemAccessories() {
        driver.get("https://cactus.kh.ua/");
        ProductListPage productListPage = new Header(driver)
                .clickItemAccessories();

        Assert.assertTrue(productListPage.contentOfCorrespondingItem.isDisplayed(), "There are some points");

    }

}
