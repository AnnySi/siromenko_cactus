package tests;

import org.junit.rules.ExpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Header;
import pages.OrderPage;
import pages.ProductListPage;
import pages.ProductPage;

public class OrderTest extends BaseTest {

    @Test(description = "Navigate To Order Page after click on Order Button")
    public void navigateToOrderPageAfterClickOrderButton() {
        driver.get("https://cactus.kh.ua/");

        ProductListPage productListPage = new Header(driver)
                .clickItemClock()
                .clickAppleWatch()
                .clickAppleWatchSeries4();
        ProductPage productPage = productListPage.clickAppleWatchSeries4WithPrice();
        OrderPage orderPageExist = productPage.clickOrderButton();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='layer_cart_product col-xs-12 col-md-8']")));

        Assert.assertTrue(orderPageExist.titleInConfirmationWindow.isDisplayed(), "Isn't displayed");
        //Assert.assertTrue(productPage.orderButton.isDisplayed(),"It isn't displayed");
    }

    @Test(description = "Check that the item has been added to the cart")
    public void checkItemAddedToCart() {
        driver.get("https://cactus.kh.ua/");

        ProductListPage productListPage = new Header(driver)
                .clickItemClock()
                .clickAppleWatch()
                .clickAppleWatchSeries4();
        ProductPage productPage = productListPage.clickAppleWatchSeries4WithPrice();
        OrderPage orderPageExist = productPage.clickOrderButton();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='layer_cart_product col-xs-12 col-md-8']")));

        Assert.assertEquals(orderPageExist.titleInConfirmationWindow.getText(), "Товар был добавлен в корзину", "Product is not added");
    }

    @Test(description = "Check order Page after click on Order Button")
    public void checkOrderPageAfterClickOrderButton() {
        driver.get("https://cactus.kh.ua/");

        ProductListPage productListPage = new Header(driver)
                .clickItemClock()
                .clickAppleWatch()
                .clickAppleWatchSeries4();
        ProductPage productPage = productListPage.clickAppleWatchSeries4WithPrice();
        OrderPage orderPage = productPage.clickOrderButton();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='layer_cart_product col-xs-12 col-md-8']")));

        Assert.assertEquals(orderPage.descriptionInItemTitleOrderPage.getText(), "Apple Watch Series 4 (GPS) 40mm Gold Aluminum Case with Pink Sand Sport Band MU682", "Product is not displayed");
    }
}
